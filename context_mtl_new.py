#!/usr/bin/env python

# Comparison
# 1. Two-task setup; Five fold CV AUC
# 2. Regular setup; Five fold CV AUC
from shogun.Features import BinaryLabels, StringCharFeatures, RealFeatures, CombinedFeatures, DNA
from shogun.Kernel import CustomKernel, CombinedKernel, WeightedDegreePositionStringKernel, GaussianKernel, VarianceKernelNormalizer, MSG_DEBUG
from shogun.Classifier import LibSVM # , LibLinear, L2R_L2LOSS_SVC
from shogun.Evaluation import ROCEvaluation, CrossValidation, CrossValidationResult, StratifiedCrossValidationSplitting
from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File

from numpy import zeros,ones,float64,int32
from numpy import array, concatenate, vstack, hstack, ones, genfromtxt, savetxt,unique, where
from numpy.random import randn, seed
from numpy.linalg import norm
import numpy

from Bio import SeqIO

import random
import os
import sys
import glob

from utils import *
import logging

logging.basicConfig( filename='mtl_mu_select.log', level=logging.DEBUG )

def seq_to_features( seq, id ):
    from shogun.Features import StringCharFeatures, RealFeatures, CombinedFeatures, DNA
    seq_len = len( seq[id[0]] )
    upstm_features = [ seq[x][0:((seq_len - 6)/2)][::-1] for x in id ] 
    dnstm_features = [ seq[x][((seq_len + 6)/2):] for x in id ] 
    upstm_features = StringCharFeatures( upstm_features, DNA )
    dnstm_features = StringCharFeatures( dnstm_features, DNA )
    stop_codon = array( [ float( x.split('|')[-4] ) for x in id ] )
    upstream   = array( [ float( x.split('|')[-3] ) for x in id ] )
    downstream = array( [ float( x.split('|')[-2] ) for x in id ] )
    position_features = vstack( ( stop_codon, upstream, downstream ) )
    # Exponential transform 
    # sigma = 2000
    # Quantile transform
    # position_features = vstack( ( exp( -( stop_codon / sigma ) ), exp( -( upstream / sigma ) ), exp( -(downstream/sigma) ) ) )
    # position_features = vstack( ( 
        # array( [ percentileofscore( stop_codon, score = x ) for x in stop_codon ]  ) / 100. , 
        # array( [ percentileofscore( upstream, score = x ) for x in upstream ]  ) / 100. , 
        # array( [ percentileofscore( downstream, score = x ) for x in downstream ]  ) / 100.
        # ) )
    position_features = RealFeatures( position_features )
    features = CombinedFeatures()
    features.append_feature_obj( upstm_features )
    features.append_feature_obj( dnstm_features )
    features.append_feature_obj( position_features )
    return features
# End

# # tcell_train_f1 = '../data/tcell.pos.no_155.with_pos.66nt.fa'
# tcell_train_f2 = '../data/tcell.neg.no_155.with_pos.66nt.fa'

tcell_train_f1 = 'data/tcell_clip_pos_new.66nt.fa'
tcell_train_f2 = 'data/tcell_clip_neg_new.66nt.fa'

# hek293_train_f1 = '../hek293_clip/hek293_clip_pos.66nt.fa'
# hek293_train_f2 = '../hek293_clip/hek293_clip_neg.66nt.fa'

hek293_train_f1 = 'data/hek293_clip_pos_new.66nt.fa'
hek293_train_f2 = 'data/hek293_clip_neg_new.66nt.fa'

# Import Tcell data
pos_seq = parse_fasta( tcell_train_f1 )
neg_seq = parse_fasta( tcell_train_f2 )
tcell_seq = dict( pos_seq.items() + neg_seq.items() )
pos_id = pos_seq.keys()
neg_id = neg_seq.keys()
tcell_id = pos_id + neg_id
tcell_labels = numpy.array( [1] * len(pos_id) + [-1]* len(neg_id))

tcell_features = seq_to_features( tcell_seq, tcell_id )

# Import HEK293 data
pos_seq = parse_fasta( hek293_train_f1 )
neg_seq = parse_fasta( hek293_train_f2 )
hek293_seq = dict( pos_seq.items() + neg_seq.items() )
pos_id = pos_seq.keys()
neg_id = neg_seq.keys()
hek293_id = pos_id + neg_id
hek293_labels = numpy.array( [1] * len(pos_id) + [-1]* len(neg_id))

hek293_features = seq_to_features( hek293_seq, hek293_id )

gamma = 1e5

kernel_pos = GaussianKernel( 10, gamma )
kernel_upstm = WeightedDegreePositionStringKernel( 10, 6 )
kernel_dnstm = WeightedDegreePositionStringKernel( 10, 6 )
kernel_upstm.set_shifts( zeros(30, dtype = int32) )
kernel_dnstm.set_shifts( zeros(30, dtype = int32) )


# Used for generating kernel matrix blocks
tkernel = CombinedKernel()
tkernel.parallel.set_num_threads(12)
tkernel.append_kernel(kernel_upstm)
tkernel.append_kernel(kernel_dnstm)
tkernel.append_kernel(kernel_pos)

# Assemble matrix blocks

tkernel.init( tcell_features, tcell_features )
block11 = tkernel.get_kernel_matrix()
tkernel.init( tcell_features, hek293_features )
block12 = tkernel.get_kernel_matrix()
block21 = block12.T
tkernel.init( hek293_features, hek293_features )
block22 = tkernel.get_kernel_matrix()

# Parameter: weight between common model and specific task
# May need lots of hand tuning
mu = 0.1
# Kernel matrix for training
K_full = vstack( (hstack( ( (1+mu) * block11, mu * block12) ), hstack( ( mu * block21, (1+mu) * block22) ) ) )
# Combined labels for training
labels = concatenate( (tcell_labels, hek293_labels) )

# Train the final SVM and save 
kernel_train = CustomKernel()
kernel_train.set_full_kernel_matrix_from_full( K_full )
labels_train = BinaryLabels( labels )
svm = LibSVM( 1., kernel_train, labels_train )
svm.set_epsilon( 1e-5 )
svm.set_bias_enabled(True)
svm.parallel.set_num_threads(12)
svm.train()

sv = svm.get_support_vectors( )
alphas = svm.get_alphas( )
savetxt( "mtl_new_mu_%.2f_alphas.txt" % mu, vstack( ( sv, alphas ) ).T, fmt = "%.8f", delimiter = "," )




# splitting_strategy=StratifiedCrossValidationSplitting(BinaryLabels(labels), 5)
# splitting_strategy.build_subsets()
# evaluator = ROCEvaluation()
# # Seems like I have to do CV by hand 
# cv_auroc = [0] * 5
# for i in range( 5 ):
    # test_idx = splitting_strategy.generate_subset_indices( i )
    # mask = ones( K_full.shape[0], dtype = bool )
    # mask[ test_idx ]  = False
    # labels_train = BinaryLabels( labels[ mask ] )
    # labels_test = BinaryLabels( labels[~mask] )
    # K_train = K_full[ mask, :] 
    # K_train = K_train[ :, mask ]
    # kernel_train = CustomKernel()
    # kernel_train.set_full_kernel_matrix_from_full( K_train )
    # K_test = K_full[ mask, :] 
    # K_test = K_test[ :, ~mask ]
    # kernel_test = CustomKernel()
    # kernel_test.set_full_kernel_matrix_from_full( K_test )
    # svm = LibSVM( 1., kernel_train, labels_train )
    # svm.set_epsilon( 1e-5 )
    # svm.set_bias_enabled(True)
    # svm.parallel.set_num_threads(20)
    # # This particular line may hurt performance ...
    # # svm.set_C( 10, 1 )
    # svm.train()
    # svm.set_kernel( kernel_test )
    # prediction = svm.apply()
    # auroc = evaluator.evaluate( prediction, labels_test )
    # cv_auroc[ i ] = auroc
# mtl_cv_auroc = numpy.mean( cv_auroc )
# logging.info( "Weight: " + str(mu) + " CV AUROC (sequence+position): "+ str(mtl_cv_auroc) )


# Comparison: Regular SVM
# K_full = vstack( (hstack( ( block11, block12) ), hstack( ( block21, block22) ) ) )
# labels = concatenate( (tcell_labels, hek293_labels) )

# splitting_strategy=StratifiedCrossValidationSplitting(BinaryLabels(labels), 5)
# splitting_strategy.build_subsets()
# evaluator = ROCEvaluation()
# Seems like I have to do CV by hand 
# cv_auroc = [0] * 5
# for i in range( 5 ):
    # test_idx = splitting_strategy.generate_subset_indices( i )
    # mask = ones( K_full.shape[0], dtype = bool )
    # mask[ test_idx ]  = False
    # labels_train = BinaryLabels( labels[ mask ] )
    # labels_test = BinaryLabels( labels[~mask] )
    # K_train = K_full[ mask, :] 
    # K_train = K_train[ :, mask ]
    # kernel_train = CustomKernel()
    # kernel_train.set_full_kernel_matrix_from_full( K_train )
    # K_test = K_full[ mask, :] 
    # K_test = K_test[ :, ~mask ]
    # kernel_test = CustomKernel()
    # kernel_test.set_full_kernel_matrix_from_full( K_test )
    # svm = LibSVM( 1., kernel_train, labels_train )
    # svm.set_epsilon( 1e-5 )
    # svm.set_bias_enabled(True)
    # svm.parallel.set_num_threads(20)
    # svm.set_C( 10, 1 )
    # svm.train()
    # svm.set_kernel( kernel_test )
    # prediction = svm.apply()
    # auroc = evaluator.evaluate( prediction, labels_test )
    # cv_auroc[ i ] = auroc

# reg_cv_auroc = numpy.mean( cv_auroc )
# logging.info( "Regular SVM" + " CV AUROC (with position): "+ str(reg_cv_auroc) )
