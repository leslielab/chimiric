#!/usr/bin/env python

import os
import os.path
import sys

if( len(sys.argv) != 5 ):
    print "Usage: " + os.path.abspath( sys.argv[0] ) + " <alignment parameters> <mRNA sequences> <microRNA sequences> <Output file>"
    sys.exit(1)

from shogun.Features import BinaryLabels, StringCharFeatures, RealFeatures, SparseRealFeatures, CombinedFeatures, DNA
from shogun.Kernel import CombinedKernel, WeightedDegreeStringKernel,LinearKernel, VarianceKernelNormalizer, MSG_DEBUG
from shogun.Classifier import SVMLin, LibLinear, L2R_L2LOSS_SVC
from shogun.Evaluation import ROCEvaluation, CrossValidation, CrossValidationResult, StratifiedCrossValidationSplitting
from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File

from numpy import zeros,ones,float64,int32
from numpy import array, concatenate, vstack, hstack, ones, genfromtxt, savetxt,unique
from numpy.random import randn, seed
from numpy.linalg import norm
import numpy

from Bio import SeqIO

import random
import glob 
from shutil import copyfile

from utils import *
from align import *


( conf_prefix, train_f, ref_f, out_prefix ) = sys.argv[1:5]
for i in range(11,16):  
    buf = run_alignment( conf_prefix = conf_prefix, train_f = train_f, ref_f = ref_f, out_prefix = out_prefix, i = i, all = True )
