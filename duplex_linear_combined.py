#!/usr/bin/env python
# Models trained solely on CLASH failed with whatever initial parameters. 
# So maybe it makes more sense to use both CLASH and CLIP data explicitly.

from shogun.Features import BinaryLabels, StringCharFeatures, RealFeatures, SparseRealFeatures, CombinedFeatures, DNA
from shogun.Kernel import CombinedKernel, WeightedDegreeStringKernel,LinearKernel, VarianceKernelNormalizer, MSG_DEBUG
from shogun.Classifier import SVMLin, LibLinear, L2R_L2LOSS_SVC
from shogun.Evaluation import ROCEvaluation, CrossValidation, CrossValidationResult, StratifiedCrossValidationSplitting
from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File

from numpy import zeros,ones,float64,int32
from numpy import array, concatenate, vstack, hstack, ones, genfromtxt, savetxt,unique
from numpy.random import randn, seed
from numpy.linalg import norm
import numpy

from Bio import SeqIO

import random
import os
import sys
from shutil import copyfile

from utils import *
from align import *
from vienna_utils import *


( aligner, conf_prefix, clash_imbalance_ratio, iter, log_f, clash_out_prefix1, clash_out_prefix2,
 clash_ref_f, svm_obj_file, clash_train_f1, clash_train_f2  ) = parse_yaml('duplex_clash_hek293.yaml')

( aligner, conf_prefix, clip_imbalance_ratio, iter, log_f, clip_out_prefix1, clip_out_prefix2,
 clip_ref_f, svm_obj_file, clip_train_f1, clip_train_f2  ) = parse_yaml('duplex_clip_hek293.yaml')

# Read fasta sequences
pos_seq = parse_fasta( clash_train_f1 )

# mapping = map_mir_to_seed_2( ref_f )

copyfile( 'hek293_top_25_seed_1mm.config.1', conf_prefix + '1' )
run_vienna( clash_train_f1, clash_ref_f, clash_out_prefix1 + '1', True )
run_vienna( clip_train_f1, clip_ref_f, clip_out_prefix1 + '1', False )
run_vienna( clip_train_f2, clip_ref_f, clip_out_prefix2 + '1', False )

# random_init( conf_prefix + '1' )

# buf = run_alignment( conf_prefix = conf_prefix, train_f = clash_train_f1, 
        # ref_f = clash_ref_f, out_prefix = clash_out_prefix1, i = 1, all = True )
# buf = run_alignment( conf_prefix = conf_prefix, train_f = clip_train_f1, 
        # ref_f = clip_ref_f, out_prefix = clip_out_prefix1, i = 1, all = False )
# buf = run_alignment( conf_prefix = conf_prefix, train_f = clip_train_f2,
        # ref_f = clip_ref_f, out_prefix = clip_out_prefix2, i = 1, all = False )

for i in range(1,iter):
    
    old_w = genfromtxt( conf_prefix+str(i), dtype = 'string' )
    feature_names = old_w[:,0]
    old_w = old_w[:,1].astype(float)
    
    # Extract positives and negatives from CLASH data
    pos_features = genfromtxt( clash_out_prefix1+str(i), delimiter = ',', dtype = 'string' )
    # Put all "masking" into one function
    ( pos_mask, neg_mask ) = generate_mask( pos_features, pos_seq, clash_ref_f )
    
    # clash_pos_duplex_features = pos_features[pos_mask, 3 : (pos_features.shape[1] -1) ].astype(float)
    clash_pos_duplex_scores = pos_features[pos_mask, 2 ].astype(float)
    clash_pos_duplex_features = pos_features[pos_mask, 3 : pos_features.shape[1] ].astype(float)
    pos_id = pos_features[pos_mask,1]

    neg_features = pos_features[neg_mask, :] 
    mask_rand = random.sample( range(len(neg_features)), int( round( clash_imbalance_ratio *len(clash_pos_duplex_features) ) ))
    neg_features = neg_features[mask_rand, :]
    neg_id = neg_features[:,1]
    # clash_neg_duplex_features = neg_features[:, 3 : (neg_features.shape[1] -1)].astype(float)
    clash_neg_duplex_scores = neg_features[:, 2 ].astype(float)
    clash_neg_duplex_features = neg_features[:, 3 : neg_features.shape[1] ].astype(float)
    # End
    
    # Extract (and sample) positives and negatives from CLIP data
    pos_features = genfromtxt( clip_out_prefix1+str(i), delimiter = ',', dtype = 'string' )
    mask_rand = random.sample( range(len(pos_features)), int( round( clip_imbalance_ratio * len(clash_pos_duplex_features)) ) )
    clip_pos_duplex_scores = pos_features[ mask_rand, 2 ].astype(float) 
    clip_pos_duplex_features = pos_features[ mask_rand, 3:pos_features.shape[1] ].astype(float) 
    # clip_pos_duplex_features = pos_features[ :, 3:pos_features.shape[1] ].astype(float) 
    
    neg_features = genfromtxt( clip_out_prefix2+str(i), delimiter = ',', dtype = 'string' )
    mask_rand = random.sample( range(len(neg_features)), int( round( clip_imbalance_ratio * len(clash_pos_duplex_features)) ) )
    clip_neg_duplex_scores = neg_features[ mask_rand, 2 ].astype(float)
    clip_neg_duplex_features = neg_features[ mask_rand, 3:neg_features.shape[1] ].astype(float)
    # clip_neg_duplex_features = neg_features[ :, 3:neg_features.shape[1] ].astype(float)
    # End
    
    labels = numpy.array( [1] * len(clash_pos_duplex_features) + [-1] *len(clash_neg_duplex_features) + [1] * len(clip_pos_duplex_features) + [-1] *len(clip_neg_duplex_features) ) 
    labels = BinaryLabels( labels )
    
    duplex_features = vstack( (clash_pos_duplex_features,
                               clash_neg_duplex_features,
                               clip_pos_duplex_features,
                               clip_neg_duplex_features ) )
    
    
    prediction = concatenate( (clash_pos_duplex_scores,
                               clash_neg_duplex_scores,
                               clip_pos_duplex_scores,
                               clip_neg_duplex_scores ) )

    prediction = BinaryLabels( prediction )
    evaluator = ROCEvaluation()
    old_auroc = evaluator.evaluate( prediction, labels )
    
    
    duplex_features = duplex_features.transpose()
    duplex_features = RealFeatures( duplex_features )
    
    
    
    linear = LibLinear( 1., duplex_features, labels )
    linear.set_liblinear_solver_type(L2R_L2LOSS_SVC)
    linear.set_epsilon(1e-6)
    linear.set_bias_enabled(True)
    # linear.set_C( 10, 10 )
    
    # SVMLin for comparison
    # duplex_features_sparse = SparseRealFeatures()
    # duplex_features_sparse.obtain_from_simple(duplex_features)
    # svm = SVMLin( 10, duplex_features_sparse, labels )
    # svm.set_epsilon( 1e-6 )
	# svm.set_bias_enabled(True)
	# svm.train()
    # svmlin_w = svm.get_w()
    # svmlin_w = svmlin_w / norm( svmlin_w )
    # 100% consistent ... So have to play with different unlabeled data setup
    
    
    splitting_strategy=StratifiedCrossValidationSplitting(labels, 5)
    evaluation_criterium = ROCEvaluation()
    cross_validation=CrossValidation(linear, duplex_features, labels, splitting_strategy, evaluation_criterium)
    cross_validation.set_autolock(False)
    result=cross_validation.evaluate()
    result = CrossValidationResult.obtain_from_generic( result )
    new_auroc = result.mean
    
    with open(log_f, "a+b") as f:
        f.write(str(i) + "\t" + str(old_auroc) + "\t" + str(new_auroc) + "\n")
    

    linear.train()
    w = linear.get_w()
    
    w = w / norm(w)
    
    w_out = ["%.6f" % x for x in w ]
    w_out = vstack(( feature_names, w_out )).transpose()
    savetxt( conf_prefix+str(i+1), w_out, fmt = '%s' )

    buf = run_alignment( conf_prefix = conf_prefix, train_f = clash_train_f1, ref_f = clash_ref_f, out_prefix = clash_out_prefix1, i = i + 1 , all = True )
    buf = run_alignment( conf_prefix = conf_prefix, train_f = clip_train_f1, ref_f = clip_ref_f, out_prefix = clip_out_prefix1, i = i + 1, all = False )
    buf = run_alignment( conf_prefix = conf_prefix, train_f = clip_train_f2, ref_f = clip_ref_f, out_prefix = clip_out_prefix2, i = i + 1, all = False )

# ( aligner, conf_prefix, imbalance_ratio, iter, log_f, out_prefix1, out_prefix2,
 # ref_f, svm_obj_file, train_f1, train_f2  ) = parse_yaml('test.yaml')

# for i in range(5,16):
    # run_alignment( aligner = aligner, conf_prefix = conf_prefix, train_f = train_f1, ref_f = ref_f, out_prefix = out_prefix1, i = i, type = 'negative' )
    # run_alignment( aligner = aligner, conf_prefix = conf_prefix, train_f = train_f2, ref_f = ref_f, out_prefix = out_prefix2, i = i, type = 'negative' )

# ( aligner, conf_prefix, imbalance_ratio, iter, log_f, out_prefix1, out_prefix2, ref_f, svm_obj_file, train_f1, train_f2  ) = parse_yaml('mir155_test.yaml')

# for i in range(5,16):
    # run_alignment( conf_prefix = conf_prefix, train_f = train_f1, ref_f = ref_f, out_prefix = out_prefix1, i = i, all = True  )
    # run_alignment( conf_prefix = conf_prefix, train_f = train_f2, ref_f = ref_f, out_prefix = out_prefix2, i = i, all = False )

# ( aligner, conf_prefix, imbalance_ratio, iter, log_f, out_prefix1, out_prefix2, ref_f, svm_obj_file, train_f1, train_f2  ) = parse_yaml('mir708_test.yaml')

# for i in range(5,11):
    # run_alignment( conf_prefix = conf_prefix, train_f = train_f1, ref_f = ref_f, out_prefix = out_prefix1, i = i, all = True )
    # run_alignment( conf_prefix = conf_prefix, train_f = train_f2, ref_f = ref_f, out_prefix = out_prefix2, i = i, all = False )


