#!/usr/bin/env python

# Add positions inferred from ApA atlas and see if there's any improvement
from shogun.Features import BinaryLabels, StringCharFeatures, RealFeatures, CombinedFeatures, DNA
from shogun.Kernel import CombinedKernel, WeightedDegreePositionStringKernel, GaussianKernel, LinearKernel, PolyKernel, VarianceKernelNormalizer, MSG_DEBUG
from shogun.Classifier import LibSVM # , LibLinear, L2R_L2LOSS_SVC
from shogun.Evaluation import ROCEvaluation, CrossValidation, CrossValidationResult, StratifiedCrossValidationSplitting
from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File

from numpy import exp, zeros,ones,float64,int32
from numpy import array, concatenate, vstack, hstack, ones, genfromtxt, savetxt,unique, where
from numpy.random import randn, seed
from numpy.linalg import norm
from scipy.stats import percentileofscore
import numpy

from Bio import SeqIO
from Bio.Seq import Seq
# from bx.bbi.bigwig_file import BigWigFile
from functools import partial

import random
import os
import sys
import glob

from utils import *
import logging

logging.basicConfig( filename='gamma_select.log', level=logging.DEBUG )

# train_f1 = '../data/tcell_pos_no_155_new.fa'
# train_f2 = '../data/tcell_neg_no_155_new.fa'

sigma = 2000.
def seq_to_features( seq, id ):
    from shogun.Features import StringCharFeatures, RealFeatures, CombinedFeatures, DNA
    seq_len = len( seq[id[0]] )
    upstm_features = [ seq[x][0:((seq_len - 6)/2)][::-1] for x in id ] 
    dnstm_features = [ seq[x][((seq_len + 6)/2):] for x in id ] 
    upstm_features = StringCharFeatures( upstm_features, DNA )
    dnstm_features = StringCharFeatures( dnstm_features, DNA )
    stop_codon = array( [ float( x.split('|')[-4] ) for x in id ] )
    upstream   = array( [ float( x.split('|')[-3] ) for x in id ] )
    downstream = array( [ float( x.split('|')[-2] ) for x in id ] )
    # No transform
    position_features = vstack( ( stop_codon,  upstream, downstream ) )
    # Exponential transform 
    # position_features = vstack( ( exp( -( stop_codon / sigma ) ), exp( -( upstream / sigma ) ), exp( -(downstream/sigma) ) ) )
    # Quantile transform
    # position_features = vstack( ( 
        # array( [ percentileofscore( stop_codon, score = x ) for x in stop_codon ]  ) / 100. , 
        # array( [ percentileofscore( upstream, score = x ) for x in upstream ]  ) / 100. , 
        # array( [ percentileofscore( downstream, score = x ) for x in downstream ]  ) / 100.
        # ) )
    position_features = RealFeatures( position_features )
    features = CombinedFeatures()
    features.append_feature_obj( upstm_features )
    features.append_feature_obj( dnstm_features )
    features.append_feature_obj( position_features )
    return features
# End
hek293_train_f1 = '../data/tcell_clip_pos_new.66nt.fa'
hek293_train_f2 = '../data/tcell_clip_neg_new.66nt.fa'

# Get a list of miR names and seeds
hek293_mir = "../hek293_clip/hek293_clip_top_mir.fa"
hek293_mir_seq = parse_fasta( hek293_mir )
hek293_mir_seed = dict( [ (x, str( Seq(hek293_mir_seq[x][1:7] ).reverse_complement() ) ) for x in hek293_mir_seq.keys() ] )
hek293_seed_mir = dict( ( v, k ) for ( k,v ) in hek293_mir_seed.iteritems() )


# Read fasta sequences
pos_seq = parse_fasta( hek293_train_f1 )
neg_seq = parse_fasta( hek293_train_f2 )
hek293_seq = dict( pos_seq.items() + neg_seq.items() )
pos_id = pos_seq.keys()
neg_id = neg_seq.keys()
hek293_id = pos_id + neg_id

hek293_seed_fam = list( unique( [ x.split( '|')[-1] for x in hek293_id ] ) )
# hek293_lomo = hek293_seed_fam[ [ hek293_seed_mir[lomo_seed] in x.split(';') for x in hek293_seed_fam ].index( True ) ]

hek293_labels = numpy.array( [1] * len(pos_id) + [-1]* len(neg_id))
labels = BinaryLabels( hek293_labels )
hek293_features = seq_to_features( hek293_seq, hek293_id )
# hek293_lomo_mask = [ x.split('|')[-1] == hek293_lomo for x in hek293_id ]

# g_range = [ 5e4, 7.5e4, 1e5, 2.5e5, 5e5, 7.5e5 ]
# for gamma in g_range:
# gamma = 5e5
gamma = 5e4
# kernel_pos = GaussianKernel( 10, gamma )
kernel_pos = PolyKernel( 10, 2 )
kernel_upstm = WeightedDegreePositionStringKernel( 10, 6 )
kernel_dnstm = WeightedDegreePositionStringKernel( 10, 6 )
kernel_upstm.set_shifts( zeros(30, dtype = int32) )
kernel_dnstm.set_shifts( zeros(30, dtype = int32) )

# Used for generating kernel matrix blocks
kernel = CombinedKernel()
kernel.parallel.set_num_threads(20)
kernel.append_kernel(kernel_upstm)
kernel.append_kernel(kernel_dnstm)
kernel.append_kernel(kernel_pos)
# kernel.set_subkernel_weights(array([ .1, .1, 1. ] ) )


svm = LibSVM( 1., kernel, labels )
svm.set_epsilon( 1e-5 )
svm.set_bias_enabled(True)
svm.parallel.set_num_threads(20)
# svm.set_C( 10, 1 )

splitting_strategy=StratifiedCrossValidationSplitting(labels, 5)
evaluation_criterium = ROCEvaluation()
cross_validation=CrossValidation(svm, hek293_features, labels, splitting_strategy, evaluation_criterium)
cross_validation.set_autolock(False)
result=cross_validation.evaluate()
result = CrossValidationResult.obtain_from_generic( result )
cv_auroc = result.mean
print "T cell CV AUROC (with position):", cv_auroc
logging.info(  "T cell CV AUROC (sequence + Polynomial position):%.8f" % ( cv_auroc ) )

# svm.train(features)
# prediction = svm.apply(features)
# evaluator = ROCEvaluation()
# auroc = evaluator.evaluate( prediction, labels )

# Save the final SVM object as a text file
# svm_save_serializable( svm, 'hek293_wdk_with_pos.', 'asc' )


# As comparison, only use WDK 
# kernel = CombinedKernel()
# kernel.append_kernel(kernel_upstm)
# kernel.append_kernel(kernel_dnstm)

# features = CombinedFeatures()
# features.append_feature_obj( upstm_features )
# features.append_feature_obj( dnstm_features )
  
# svm = LibSVM( 1., kernel, labels )
# svm.set_epsilon( 1e-5 )
# svm.set_bias_enabled(True)
# svm.parallel.set_num_threads(20)
# svm.set_C( 10, 1 )

# splitting_strategy=StratifiedCrossValidationSplitting(labels, 5)
# evaluation_criterium = ROCEvaluation()
# cross_validation=CrossValidation(svm, features, labels, splitting_strategy, evaluation_criterium)
# cross_validation.set_autolock(False)
# result=cross_validation.evaluate()
# result = CrossValidationResult.obtain_from_generic( result )
# cv_auroc = result.mean
# print "CV AUROC (without position):", cv_auroc

# svm.train(features)
# prediction = svm.apply(features)
# evaluator = ROCEvaluation()
# auroc = evaluator.evaluate( prediction, labels )

# Save the final SVM object as a text file
# svm_save_serializable( svm, 'hek293_wdk_without_pos.', 'asc' )
