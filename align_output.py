import re 

class AlignmentOutput(object):

    def __init__( self, mrna_id = '', mirna_id = '', \
            mrna_seq = '', mirna_seq = '', \
            best_hybrid_score = 0, 
            mrna_alignment = '', mirna_alignment = '', alignment = '' ):
        self.mrna_id, self.mirna_id, self.mrna_seq, self.mirna_seq = \
                mrna_id, mirna_id, mrna_seq, mirna_seq
        self.best_hybrid_score = best_hybrid_score
        self.mrna_alignment, self.mirna_alignment, self.alignment = \
                mrna_alignment, mirna_alignment, alignment
        self.feature_count = dict()

    def featureCount(self):
        score_recover = 0 
        mirna_len = len( re.findall( '[ACGT]', self.mirna_alignment ) )
        for i in range( mirna_len ):
            self.feature_count[ i ] = 0 
        for i in ( 'GT', 'TG', 'AT', 'TA', 'GC', 'CG' ):
            self.feature_count[ i ] = 0 
        self.feature_count[ 'open' ] = len( re.findall( '\|[Ov^]', self.alignment ) )
        self.feature_count[ 'sym' ] = self.alignment.count( 'O' )
        self.feature_count[ 'mirna' ] = self.alignment.count( '^' )
        self.feature_count[ 'mrna' ] = self.alignment.count( 'v' )

        mirna_start = self.alignment.index( '|' )
        mirna_end = self.alignment.rindex( '|' )

        for i in range( mirna_start, mirna_end + 1 ):
            align_sub = self.alignment[ 0: i + 1 ]
            mirna_sub = self.mirna_alignment[ 0: i + 1 ]
            offset = mirna_sub.count( '-' )
            if ( align_sub[-1] == '|' ):
                pair = self.mirna_alignment[i] + self.mrna_alignment[i]
                self.feature_count[ i - offset ] += 1
                if i != offset:
                    self.feature_count[ pair ] += 1

    def sanity( self, positional_score ):
        # Sanity check
        score_recover = sum( [ self.feature_count[ x ] * positional_score[x] for x in self.feature_count.keys()  ] )
        assert abs( score_recover - self.best_hybrid_score ) < 1e-6, "%d vs %d: alignment scores not consistent!" % ( score_recover, self.best_hybrid_score)

    def display( self ):
        return "%s <-> %s: %.3f\n%s\n%s\n%s\n" % ( self.mirna_id, self.mrna_id, self.best_hybrid_score, self.mirna_alignment, self.alignment, self.mrna_alignment )

    def printFeature( self ):
        mirna_len = len( re.findall( '[ACGT]', self.mirna_alignment ) )
        feat_pair = ",".join( [ str( self.feature_count[x] ) for x in ( 'GT', 'TG', 'AT', 'TA', 'GC', 'CG' ) ] )
        feat_gap = ",".join( [ str( self.feature_count[x] ) for x in ( 'open', 'sym', 'mirna', 'mrna' ) ] )
        feat_position = ",".join( [ str( self.feature_count[x] ) for x in range( mirna_len ) ] ) 
        return ",".join( [ self.mirna_id, self.mrna_id, "%.6f" % self.best_hybrid_score, feat_pair, feat_gap, feat_position ] )

    def setName( self, mirna_id, mrna_id ): 
        self.mirna_id = mirna_id
        self.mrna_id = mrna_id

    def getName( self ):
        return( self.mirna_id, self.mrna_id )
