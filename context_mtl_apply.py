#!/usr/bin/env python

from shogun.Features import BinaryLabels, StringCharFeatures, RealFeatures, CombinedFeatures, DNA
from shogun.Kernel import CustomKernel, CombinedKernel, WeightedDegreePositionStringKernel, GaussianKernel, VarianceKernelNormalizer, MSG_DEBUG
from shogun.Classifier import LibSVM # , LibLinear, L2R_L2LOSS_SVC
from shogun.Evaluation import ROCEvaluation, CrossValidation, CrossValidationResult, StratifiedCrossValidationSplitting
from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File

from numpy import zeros,ones,float64,int32
from numpy import array, concatenate, vstack, hstack, ones, genfromtxt, savetxt,unique, where
from numpy.random import randn, seed
from numpy.linalg import norm
import numpy

from Bio import SeqIO

import random
import os
import sys
import glob

from utils import *


# From the sequence dict to the CombinedFeatures object
def seq_to_features( seq, id ):
    from shogun.Features import StringCharFeatures, RealFeatures, CombinedFeatures, DNA
    seq_len = len( seq[id[0]] )
    upstm_features = [ seq[x][0:((seq_len - 6)/2)][::-1] for x in id ] 
    dnstm_features = [ seq[x][((seq_len + 6)/2):] for x in id ] 
    upstm_features = StringCharFeatures( upstm_features, DNA )
    dnstm_features = StringCharFeatures( dnstm_features, DNA )
    stop_codon = array( [ float( x.split('|')[-4] ) for x in id ] )
    upstream   = array( [ float( x.split('|')[-3] ) for x in id ] )
    downstream = array( [ float( x.split('|')[-2] ) for x in id ] )
    position_features = vstack( ( stop_codon, upstream, downstream ) )
    # Exponential transform 
    # sigma = 2000
    # Quantile transform
    # position_features = vstack( ( exp( -( stop_codon / sigma ) ), exp( -( upstream / sigma ) ), exp( -(downstream/sigma) ) ) )
    # position_features = vstack( ( 
        # array( [ percentileofscore( stop_codon, score = x ) for x in stop_codon ]  ) / 100. , 
        # array( [ percentileofscore( upstream, score = x ) for x in upstream ]  ) / 100. , 
        # array( [ percentileofscore( downstream, score = x ) for x in downstream ]  ) / 100.
        # ) )
    position_features = RealFeatures( position_features )
    features = CombinedFeatures()
    features.append_feature_obj( upstm_features )
    features.append_feature_obj( dnstm_features )
    features.append_feature_obj( position_features )
    return features
# End
# tcell_train_f1 = '../data/tcell.pos.no_155.with_pos.66nt.fa'
# tcell_train_f2 = '../data/tcell.neg.no_155.with_pos.66nt.fa'

# hek293_train_f1 = '../hek293_clip/hek293_clip_pos.66nt.fa'
# hek293_train_f2 = '../hek293_clip/hek293_clip_neg.66nt.fa'

tcell_train_f1 = '../data/tcell_clip_pos_new.66nt.fa'
tcell_train_f2 = '../data/tcell_clip_neg_new.66nt.fa'

hek293_train_f1 = '../hek293_clip/hek293_clip_pos_new.66nt.fa'
hek293_train_f2 = '../hek293_clip/hek293_clip_neg_new.66nt.fa'

test_f = "../data/hela_clip_top_sites_new.66nt.fa"

# Import Tcell data
pos_seq = parse_fasta( tcell_train_f1 )
neg_seq = parse_fasta( tcell_train_f2 )
tcell_seq = dict( pos_seq.items() + neg_seq.items() )
pos_id = pos_seq.keys()
neg_id = neg_seq.keys()
tcell_id = pos_id + neg_id
tcell_labels = numpy.array( [1] * len(pos_id) + [-1]* len(neg_id))

tcell_features = seq_to_features( tcell_seq, tcell_id )

# Import HEK293 data
pos_seq = parse_fasta( hek293_train_f1 )
neg_seq = parse_fasta( hek293_train_f2 )
hek293_seq = dict( pos_seq.items() + neg_seq.items() )
pos_id = pos_seq.keys()
neg_id = neg_seq.keys()
hek293_id = pos_id + neg_id
hek293_labels = numpy.array( [1] * len(pos_id) + [-1]* len(neg_id))

hek293_features = seq_to_features( hek293_seq, hek293_id )


test_seq = parse_fasta( test_f )
test_id  = test_seq.keys()
test_features = seq_to_features( test_seq, test_id )

# Construct kernel matrix
gamma = 1e5
kernel_upstm = WeightedDegreePositionStringKernel( 10, 6 )
kernel_dnstm = WeightedDegreePositionStringKernel( 10, 6 )
kernel_upstm.set_shifts( zeros(30, dtype = int32) )
kernel_dnstm.set_shifts( zeros(30, dtype = int32) )
kernel_pos = GaussianKernel( 10, gamma )
tkernel = CombinedKernel()
tkernel.parallel.set_num_threads(12)
tkernel.append_kernel( kernel_upstm )
tkernel.append_kernel( kernel_dnstm )
tkernel.append_kernel( kernel_pos )
tkernel.init( tcell_features, test_features )
blockt1 = tkernel.get_kernel_matrix()
tkernel.init( hek293_features, test_features )
blockt2 = tkernel.get_kernel_matrix()
K_test = vstack( (  blockt1, blockt2 ) )
kernel_test = CustomKernel()
kernel_test.set_full_kernel_matrix_from_full( K_test )

# Import MTL learned from T cell + HEK293
mtl_svm_full = numpy.loadtxt( '../config/mtl_new_mu_0.20_alphas.txt', delimiter = ',' )
mtl_sv = mtl_svm_full[:,0].astype( 'int32' )
mtl_alphas = mtl_svm_full[:,1]

svm = LibSVM()
svm.parallel.set_num_threads(12)
svm.set_support_vectors( mtl_sv )
svm.set_alphas( mtl_alphas )
svm.set_kernel( kernel_test )
prediction = svm.apply()

result = vstack( ( test_id, prediction.get_values().astype(str) ) ).T
savetxt( '../data/hela_clip_top_sites_new_66nt.txt', result, fmt = '%s' )
