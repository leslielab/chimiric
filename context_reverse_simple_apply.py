#!/usr/bin/env python

import sys
import random
import os
import os.path
import glob

if( len(sys.argv) != 4 ):
    print "Usage: " + os.path.abspath( sys.argv[0] ) + " <mRNA sequences> <SVM file> <output file>"
    sys.exit(1)

from shogun.Features import BinaryLabels, StringCharFeatures, RealFeatures, CombinedFeatures, DNA
from shogun.Kernel import CombinedKernel, WeightedDegreePositionStringKernel,LinearKernel, VarianceKernelNormalizer, MSG_DEBUG
from shogun.Classifier import LibSVM # , LibLinear, L3R_L2LOSS_SVC
from shogun.Evaluation import ROCEvaluation, CrossValidation, CrossValidationResult, StratifiedCrossValidationSplitting
from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File

from numpy import zeros,ones,float64,int32
from numpy import array, concatenate, vstack, hstack, ones, genfromtxt, savetxt,unique, where
from numpy.random import randn, seed
from numpy.linalg import norm
import numpy

from Bio import SeqIO
# from bx.bbi.bigwig_file import BigWigFile

from utils import *

( fasta_file, svm_file, out_file ) = sys.argv[ 1:4 ]

seq = parse_fasta( fasta_file )
id = seq.keys()

seq_len = len( seq[id[0]] )

svm_fmt = svm_file.split('.')[-1]
svm = svm_load_serializable( svm_file[:-len( svm_fmt )], svm_fmt )
with_pos = True if svm.get_kernel().get_num_subkernels() == 3 else False

upstm_features = [ seq[x][0:((seq_len - 6)/2)][::-1] for x in  id ] 
dnstm_features = [ seq[x][((seq_len + 6)/2):seq_len] for x in id ] 
upstm_features = StringCharFeatures( upstm_features, DNA )
dnstm_features = StringCharFeatures( dnstm_features, DNA )

features = CombinedFeatures()
features.append_feature_obj( upstm_features )
features.append_feature_obj( dnstm_features )

if( with_pos ):
    from_start = [ float( x.split('|')[-3] ) for x in id ]
    from_end   = [ float( x.split('|')[-2] ) for x in id ]
    position_features = vstack( (from_start, from_end ) )
    position_features = RealFeatures( position_features )
    features.append_feature_obj( position_features )


prediction = svm.apply( features )
prediction = prediction.get_values()

with open( out_file, 'w' ) as f:
    for x in range( len(id) ):
        f.write( "%s,%.6f\n" % ( id[x], prediction[x] ) )
