from align_output import AlignmentOutput

# Speed up loops with Cython 
cimport cython
cimport cython.view
# @cython.boundscheck(False)
# @cython.wraparound(False)
import re


# Supposed to be constants
cdef int HYBRID = 0, SYM = 1, MRNA = 2, MIRNA = 3, START = 4

cdef int *hybrid_states = [ START, HYBRID, SYM, MRNA, MIRNA ]
cdef int *sym_states = [ HYBRID, SYM ]
cdef int *mrna_states = [ HYBRID, SYM, MRNA ]
cdef int *mirna_states = [ HYBRID, SYM, MIRNA ]

# Just set them as large values so that index never exceeds
mirna_len = 21
mrna_len = 150
# Maximum gap size to avoid artifacts
max_gap = 12
top_hybrid_n = 10

# Set up three-dimension arrays

cdef double [:,:,:] best_hybrid = cython.view.array( shape=( mirna_len, mrna_len, 4 ), itemsize=sizeof(double), format="d" )
cdef int [:,:,:] traceback_i = cython.view.array( shape=( mirna_len, mrna_len, 4 ), itemsize=sizeof(int), format="i" )
cdef int [:,:,:] traceback_j = cython.view.array( shape=( mirna_len, mrna_len, 4 ), itemsize=sizeof(int), format="i" )
cdef int [:,:,:] traceback_state = cython.view.array( shape=( mirna_len, mrna_len, 4 ), itemsize=sizeof(int), format="i" )

# Need to figure out a better way to initialize parameters


class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False
    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration
    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False


cdef int max_index( double [:] x, int array_len ):
    cdef int i
    cdef int max_idx = 0
    for i in range( array_len ):
        if x[i] >= x[ max_idx ]: max_idx = i
    return max_idx
    
def run_viterbi( mirna_seq, mrna_seq, positional_score,  bp_score ):
    cdef int i, j, best_hybrid_i, best_hybrid_j, \
            max_hybrid_idx, max_sym_idx, max_mirna_idx, max_mrna_idx
    cdef double best_hybrid_score, max_hybrid, max_sym, max_mirna, max_mrna

    cdef double hybrid_score_value
    cdef double open_score_value = positional_score['open']
    cdef double sym_score_value = positional_score['sym']
    cdef double mirna_score_value = positional_score['mirna']
    cdef double mrna_score_value = positional_score['mrna']

    cdef double [:] hybrid_scores = cython.view.array( shape=(5,), itemsize=sizeof(double), format="d" )
    cdef double [:] sym_scores = cython.view.array( shape=(2,), itemsize=sizeof(double), format="d" )
    cdef double [:] mirna_scores = cython.view.array( shape=(3,), itemsize=sizeof(double), format="d" )
    cdef double [:] mrna_scores = cython.view.array( shape=(3,), itemsize=sizeof(double), format="d" )

    mirna_seq = mirna_seq.replace( 'U', 'T' )
    mrna_seq = mrna_seq.replace( 'U', 'T' )
    mrna_seq = mrna_seq[::-1]
    best_hybrid_i = 0
    best_hybrid_j = 0
    best_hybrid_score = float('-inf')
    for i in range( len(mirna_seq) ):
        for j in range( len( mrna_seq ) ):
            mirna_base = mirna_seq[i]
            mrna_base = mrna_seq[j]
            pair = mirna_base + mrna_base
            hybrid_score_value = bp_score[i][pair] if pair in bp_score[i] else float('-inf')
            if i != 0 and j != 0:
                # hybrid_scores = [
                        # 0,
                        # best_hybrid[ i - 1 ][ j - 1 ][ HYBRID ],
                        # best_hybrid[ i - 1 ][ j - 1 ][ SYM ],
                        # best_hybrid[ i - 1 ][ j - 1 ][ MRNA ],
                        # best_hybrid[ i - 1 ][ j - 1 ][ MIRNA ]
                        # ] 
                hybrid_scores[0] =  0
                hybrid_scores[1] =  best_hybrid[ i - 1 ][ j - 1 ][ HYBRID ]
                hybrid_scores[2] =  best_hybrid[ i - 1 ][ j - 1 ][ SYM ]
                hybrid_scores[3] =  best_hybrid[ i - 1 ][ j - 1 ][ MRNA ]
                hybrid_scores[4] =  best_hybrid[ i - 1 ][ j - 1 ][ MIRNA ]

                # sym_scores =  [
                        # open_score_value + best_hybrid[ i - 1 ][ j - 1 ][ HYBRID ],
                        # best_hybrid[ i - 1 ][ j - 1 ][ SYM ]
                        # ] 

                sym_scores[0] =  open_score_value + best_hybrid[ i - 1 ][ j - 1 ][ HYBRID ]
                sym_scores[1] =  best_hybrid[ i - 1 ][ j - 1 ][ SYM ]

                # mrna_scores =  [
                        # open_score_value + best_hybrid[ i ][ j - 1 ][ HYBRID ], 
                        # best_hybrid[ i ][ j - 1 ][ SYM ], 
                        # best_hybrid[ i ][ j - 1 ][ MRNA ]
                        # ] 

                mrna_scores[0] =  open_score_value + best_hybrid[ i ][ j - 1 ][ HYBRID ]
                mrna_scores[1] =  best_hybrid[ i ][ j - 1 ][ SYM ]
                mrna_scores[2] =  best_hybrid[ i ][ j - 1 ][ MRNA ]

                # mirna_scores =  [
                        # open_score_value + best_hybrid[ i - 1 ][ j ][ HYBRID ],
                        # best_hybrid[ i - 1 ][ j ][ SYM ],
                        # best_hybrid[ i - 1 ][ j ][ MIRNA ]
                        # ] 

                mirna_scores[0] =  open_score_value + best_hybrid[ i - 1 ][ j ][ HYBRID ]
                mirna_scores[1] =  best_hybrid[ i - 1 ][ j ][ SYM ]
                mirna_scores[2] =  best_hybrid[ i - 1 ][ j ][ MIRNA ]

                # max_hybrid = max( hybrid_scores )
                # max_hybrid_idx = hybrid_scores.index(max_hybrid)
                max_hybrid_idx = max_index( hybrid_scores, 5 )
                max_hybrid = hybrid_scores[ max_hybrid_idx ]
                traceback_i[ i ][ j ][ HYBRID ] = i - 1
                traceback_j[ i ][ j ][ HYBRID ] = j - 1
                traceback_state[ i ][ j ][ HYBRID ] = hybrid_states[ max_hybrid_idx ]

                # max_sym = max( sym_scores )
                # max_sym_idx = sym_scores.index( max_sym )
                max_sym_idx = max_index( sym_scores, 2 )
                max_sym = sym_scores[ max_sym_idx ]
                traceback_i[ i ][ j ][ SYM ] = i - 1
                traceback_j[ i ][ j ][ SYM ] = j - 1
                traceback_state[ i ][ j ][ SYM ] = sym_states[ max_sym_idx ]

                # max_mrna = max( mrna_scores )
                # max_mrna_idx = mrna_scores.index( max_mrna )
                max_mrna_idx = max_index( mrna_scores, 3 )
                max_mrna = mrna_scores[ max_mrna_idx ]
                traceback_i[ i ][ j ][ MRNA ] = i 
                traceback_j[ i ][ j ][ MRNA ] = j - 1
                traceback_state[ i ][ j ][ MRNA ] = mrna_states[ max_mrna_idx ]

                # max_mirna = max( mirna_scores )
                # max_mirna_idx = mirna_scores.index( max_mirna )
                max_mirna_idx = max_index( mirna_scores, 3 )
                max_mirna = mirna_scores[ max_mirna_idx ]
                traceback_i[ i ][ j ][ MIRNA ] = i - 1
                traceback_j[ i ][ j ][ MIRNA ] = j 
                traceback_state[ i ][ j ][ MIRNA ] = mirna_states[ max_mirna_idx ]
            elif i == 0 and j != 0:
                # mrna_scores =  [
                        # open_score_value + best_hybrid[i][j - 1][HYBRID],
                        # best_hybrid[i][j - 1][SYM],
                        # best_hybrid[i][j - 1][MRNA]
                        # ] 

                mrna_scores[0] =  open_score_value + best_hybrid[ i ][ j - 1 ][ HYBRID ]
                mrna_scores[1] =  best_hybrid[ i ][ j - 1 ][ SYM ]
                mrna_scores[2] =  best_hybrid[ i ][ j - 1 ][ MRNA ]

                max_hybrid = 0
                traceback_i[i][j][ HYBRID ] = i
                traceback_j[i][j][ HYBRID ] = j - 1
                traceback_state[i][j][ HYBRID ] = START

                max_sym = float('-inf')
                traceback_i[i][j][ SYM ] = i
                traceback_j[i][j][ SYM ] = j - 1
                traceback_state[i][j][ SYM ] = START

                # max_mrna = max( mrna_scores )
                # max_mrna_idx = mrna_scores.index( max_mrna )
                max_mrna_idx = max_index( mrna_scores, 3 )
                max_mrna = mrna_scores[ max_mrna_idx ]
                traceback_i[ i ][ j ][ MRNA ] = i 
                traceback_j[ i ][ j ][ MRNA ] = j - 1
                traceback_state[ i ][ j ][ MRNA ] = mrna_states[ max_mrna_idx ]

                max_mirna = float('-inf')
                traceback_i[i][j][ MIRNA ] = i
                traceback_j[i][j][ MIRNA ] = j - 1
                traceback_state[i][j][ MIRNA ] = START
            elif i != 0 and j == 0 :
                # mirna_scores =  [
                        # open_score_value + best_hybrid[ i - 1 ][ j ][ HYBRID ],
                        # best_hybrid[ i - 1 ][ j ][ SYM ],
                        # best_hybrid[ i - 1 ][ j ][ MIRNA ]
                        # ] 

                mirna_scores[0] =  open_score_value + best_hybrid[ i - 1 ][ j ][ HYBRID ]
                mirna_scores[1] =  best_hybrid[ i - 1 ][ j ][ SYM ]
                mirna_scores[2] =  best_hybrid[ i - 1 ][ j ][ MIRNA ]

                max_hybrid = 0
                traceback_i[i][j][ HYBRID ] = i - 1
                traceback_j[i][j][ HYBRID ] = j
                traceback_state[i][j][ HYBRID ] = START

                max_sym = float('-inf')
                traceback_i[i][j][ SYM ] = i - 1
                traceback_j[i][j][ SYM ] = j
                traceback_state[i][j][ SYM ] = START

                max_mrna = float('-inf')
                traceback_i[i][j][ MRNA ] = i - 1
                traceback_j[i][j][ MRNA ] = j 
                traceback_state[i][j][ MRNA ] = START

                # max_mirna = max( mirna_scores )
                # max_mirna_idx = mirna_scores.index( max_mirna )
                max_mirna_idx = max_index( mirna_scores, 3 )
                max_mirna = mirna_scores[ max_mirna_idx ]
                traceback_i[ i ][ j ][ MIRNA ] = i - 1
                traceback_j[ i ][ j ][ MIRNA ] = j
                traceback_state[ i ][ j ][ MIRNA ] = mirna_states[ max_mirna_idx ]
            else:
                max_hybrid = 0
                traceback_i[i][j][ HYBRID ] = i
                traceback_j[i][j][ HYBRID ] = j 
                traceback_state[i][j][ HYBRID ] = START

                max_sym = float('-inf')
                traceback_i[i][j][ SYM ] = i
                traceback_j[i][j][ SYM ] = j 
                traceback_state[i][j][ SYM ] = START

                max_mrna = float('-inf')
                traceback_i[i][j][ MRNA ] = i 
                traceback_j[i][j][ MRNA ] = j 
                traceback_state[i][j][ MRNA ] = START

                max_mirna = float('-inf')
                traceback_i[i][j][ MIRNA ] = i
                traceback_j[i][j][ MIRNA ] = j 
                traceback_state[i][j][ MIRNA ] = START

            best_hybrid[i][j][HYBRID] = hybrid_score_value + max_hybrid
            best_hybrid[i][j][SYM] = sym_score_value + max_sym
            best_hybrid[i][j][MRNA] = mrna_score_value + max_mrna
            best_hybrid[i][j][MIRNA] = mirna_score_value + max_mirna
            if ( best_hybrid_score <= best_hybrid[i][j][HYBRID] ):
                best_hybrid_i, best_hybrid_j, best_hybrid_score = \
                i, j, best_hybrid[i][j][HYBRID]
    # traceBack function should return an object containing sequence and alignment
    # Need to define a class?
    mrna_seq = mrna_seq[::-1]
    ( best_hybrid_score, mirna_alignment, alignment, mrna_alignment ) = choose_best_hybrid( mirna_seq, mrna_seq, positional_score )

    # ( mirna_alignment, alignment, mrna_alignment ) = traceBack( mirna_seq, mrna_seq, best_hybrid_i, best_hybrid_j, best_hybrid_score, positional_score )
    align_result = AlignmentOutput( 
            best_hybrid_score = best_hybrid_score, \
            mirna_alignment = mirna_alignment, \
            alignment = alignment, \
            mrna_alignment = mrna_alignment ) 
    return align_result
    # return ( best_hybrid_score, feature_count )


def traceBack( mirna, mrna, best_hybrid_i, best_hybrid_j, best_hybrid_score,\
            positional_score ):
    trace_i, trace_j = best_hybrid_i, best_hybrid_j
    cdef int current_mode, i, j 
    current_mode = HYBRID
    mirna_alignment, alignment, mrna_alignment = '', '', ''
    while current_mode != START:
        (mirna_alignment, alignment, mrna_alignment) = make_alignment( mirna, mrna, trace_i, trace_j, current_mode, mirna_alignment, alignment, mrna_alignment );
        i = trace_i
        j = trace_j
        trace_i = traceback_i[i][j][current_mode]
        trace_j = traceback_j[i][j][current_mode]
        current_mode = traceback_state[i][j][current_mode]
        
    # reverse the alignment as we were tracebacking...
    mirna_alignment, alignment, mrna_alignment = [ x[::-1] for x in (mirna_alignment, alignment, mrna_alignment) ]
    # Format the alignment strings
    # First reverse mRNA sequence
    mrna = mrna[::-1]
    
    # padding in the beginning
    mirna_head_padding = i 
    mrna_head_padding = j
    max_head_padding = max( i, j )
    
    alignment = '#' * max_head_padding + alignment
    mirna_alignment = '-' * ( max_head_padding - mirna_head_padding ) + mirna[ 0 : mirna_head_padding ] + mirna_alignment 
    mrna_alignment = '-' * ( max_head_padding - mrna_head_padding ) + mrna[ 0 : mrna_head_padding ] + mrna_alignment
    
    # padding in the end
    mirna_tail_padding = len( mirna ) - best_hybrid_i - 1
    mrna_tail_padding = len( mrna ) - best_hybrid_j - 1
    max_tail_padding = max( mirna_tail_padding, mrna_tail_padding )
    alignment = alignment + '#' * max_tail_padding
    mirna_alignment = mirna_alignment + \
    mirna[ best_hybrid_i + 1 : ] + \
    '-' * ( max_tail_padding - mirna_tail_padding )
    mrna_alignment = mrna_alignment + \
    mrna[ best_hybrid_j + 1 : ] + \
    '-' * ( max_tail_padding - mrna_tail_padding )
    # Figure out where to put these
    # print "\n%s\n%s\n%s\n" % ( mirna_alignment, alignment, mrna_alignment )

    # feature_count = featureCount( mirna_alignment, alignment, mrna_alignment, best_hybrid_score, positional_score )
    return ( mirna_alignment, alignment, mrna_alignment ) 
    
    
    
def make_alignment( mirna_seq, mrna_seq, i, j, mode, mirna_alignment, alignment, mrna_alignment ):
    mrna_seq = mrna_seq[::-1]
    if mode == START: mode = HYBRID
    # Python does not have a switch statement ... find a solution online but need to check if it works 
    for case in switch(mode):
        if case( HYBRID ):
            mirna_alignment = mirna_alignment + mirna_seq[ i ]
            mrna_alignment = mrna_alignment + mrna_seq[ j ]
            alignment = alignment + '|'
            break 
        if case( SYM ):
            mirna_alignment = mirna_alignment + mirna_seq[ i ]
            mrna_alignment = mrna_alignment + mrna_seq[ j ]
            alignment = alignment + 'O'
            break 
        if case( MRNA ):
            mirna_alignment = mirna_alignment + '-'
            mrna_alignment = mrna_alignment + mrna_seq[ j ]
            alignment = alignment + 'v'
            break
        if case( MIRNA ):
            mirna_alignment = mirna_alignment + mirna_seq[ i ]
            mrna_alignment = mrna_alignment + '-'
            alignment = alignment + '^'
            break
        if case():
            raise Exception( "Invalid mode parameter %d" % mode )
        
    return ( mirna_alignment, alignment, mrna_alignment )
 
# def featureCount( mirna_alignment, alignment, mrna_alignment, best_hybrid_score, positional_score ):
    # feature_count = dict()
    # score_recover = 0 
    # for x in positional_score.keys():
        # feature_count[x] = 0
    # 
    # feature_count[ 'open' ] = len( re.findall( '\|[Ov^]', alignment ) )
    # feature_count[ 'sym' ] = alignment.count( 'O' )
    # feature_count[ 'mirna' ] = alignment.count( '^' )
    # feature_count[ 'mrna' ] = alignment.count( 'v' )
    # 
    # mirna_start = alignment.index( '|' )
    # mirna_end = alignment.rindex( '|' )
    # for i in range( mirna_start, mirna_end + 1 ):
        # align_sub = alignment[ 0: i + 1 ]
        # mirna_sub = mirna_alignment[ 0: i + 1 ]
        # offset = mirna_sub.count( '-' )
        # if ( align_sub[-1] == '|' ):
            # pair = mirna_alignment[i] + mrna_alignment[i]
            # feature_count[ i - offset ] += 1
            # if i != offset:
                # feature_count[ pair ] += 1
    # score_recover = sum( [ feature_count[ x ] * positional_score[x] for x in feature_count.keys()  ] )
    
    # Sanity check
    # assert abs( score_recover - best_hybrid_score ) < 1e-6, "%d vs %d: alignment scores not consistent!" % ( score_recover, best_hybrid_score)
    # Print & return final values 
    # return feature_count

def choose_best_hybrid( mirna_seq, mrna_seq, positional_score ):
    cdef int i, j
    flatten = [ 0 ] * ( len( mirna_seq ) * len( mrna_seq ) )
    for i in range( len(mirna_seq) ):
        for j in range( len( mrna_seq ) ):
            flatten[ i * len( mrna_seq ) + j ] = best_hybrid[i][j][HYBRID]
    top_hybrid_idx = sorted( range(len(flatten)), key = lambda i: flatten[i], reverse = True )[ : top_hybrid_n ]
    for i in range( top_hybrid_n ): 
        best_hybrid_i, best_hybrid_j = divmod( top_hybrid_idx[ i ], len( mrna_seq ) )
        best_hybrid_score = flatten[ top_hybrid_idx[ i ] ]
        ( mirna_alignment, alignment, mrna_alignment ) = traceBack( mirna_seq, mrna_seq, best_hybrid_i, best_hybrid_j, best_hybrid_score, positional_score )
        # Count gap lengths
        try:
            gap_size = max( [ len(x) for x in re.findall( '[O^]+', alignment ) ] )
        except ValueError:
            gap_size = 0
        # gap_size = alignment.count('O') + alignment.count('v') + alignment.count('^')
        if gap_size <= max_gap: 
            break
        if i == (top_hybrid_n-1):
            # If no top n hybrid meets the gap size limit, give up and just use the one with the highest score
            # Maybe longest single loop makes more sense than the sum; Need to implement ...
            best_hybrid_i, best_hybrid_j = divmod( top_hybrid_idx[ 0 ], len( mrna_seq ) )
            best_hybrid_score = flatten[ top_hybrid_idx[ 0 ] ]
            ( mirna_alignment, alignment, mrna_alignment ) = traceBack( mirna_seq, mrna_seq, best_hybrid_i, best_hybrid_j, best_hybrid_score, positional_score )
    return ( best_hybrid_score, mirna_alignment, alignment, mrna_alignment )

