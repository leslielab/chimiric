#!/usr/bin/env python
# Utility functions for the microRNA target prediction project
from numpy import zeros,ones,float64,int32
from numpy import array, where, concatenate, vstack, hstack, zeros, ones, genfromtxt, savetxt,unique, tile, delete
from numpy.random import randn, seed
from numpy.linalg import norm
import numpy

from Bio import SeqIO

import random
import os
import sys


def parse_fasta( file_name ):
    handle = open( file_name, 'rU' )
    seq = SeqIO.to_dict(SeqIO.parse(handle, "fasta"))
    handle.close()
    # U letters in RNA sequences converted to T in this step
    seq = dict( [ (x, str( seq[x].seq.back_transcribe( ) ) ) for x in seq.keys() ] )
    return seq

# From the sequence dict to the CombinedFeatures object
def seq_to_features( seq, id ):
    from shogun.Features import StringCharFeatures, RealFeatures, CombinedFeatures, DNA
    seq_len = len( seq[id[0]] )
    upstm_features = [ seq[x][0:((seq_len - 6)/2)][::-1] for x in id ] 
    dnstm_features = [ seq[x][((seq_len + 6)/2):] for x in id ] 
    upstm_features = StringCharFeatures( upstm_features, DNA )
    dnstm_features = StringCharFeatures( dnstm_features, DNA )
    from_start = [ float( x.split('|')[-3] ) for x in id ]
    from_end   = [ float( x.split('|')[-2] ) for x in id ]
    position_features = vstack( (from_start, from_end ) )
    position_features = RealFeatures( position_features )
    features = CombinedFeatures()
    features.append_feature_obj( upstm_features )
    features.append_feature_obj( dnstm_features )
    features.append_feature_obj( position_features )
    return features
# End
def get_phastcons_score( bw_obj, seq_info ):
    seq_info = [  id.split('|') for id in seq_info ]
    seq_info = array(seq_info)
    seq_scores = dict()
    for id in seq_info:
        # Indexing issue here: 0-based vs 1-based
        scores = bw_obj[id[2]].get_as_array( id[2], int(id[4]) - 1, int(id[5]) )
        # Reverse the vector for negative strand
        if id[3] == '-': scores = scores[::-1]     
        scores = scores.astype('float')
        # Never forget to get rid of the Nans
        scores[numpy.isnan(scores)] = 0
        seq_scores['|'.join(id)] = scores
    return seq_scores

def svm_save_serializable( svm, svm_obj_file, suffix ):
    from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File
    # Save the trained SVM object as a text file
    # Will implement different formats once I figure out how to do it
    fstream = SerializableAsciiFile(svm_obj_file+suffix, "w")
    status = svm.save_serializable(fstream)
    # silent...
    assert(status)
    return status

def map_mir_to_seed( seed_classes ):
    # Map each microRNA to a group of microRNAs with identical seed
    # Should generate directly from microRNA sequences in the future
    from numpy import unique, tile
    seed_classes = unique( seed_classes )
    seed_classes = [ x.split(';') for x in seed_classes ]
    mir = sum( seed_classes, [] )
    seed_classes = [ tile(x, (len(x),1) ).tolist() for x in seed_classes]
    seed_classes = sum( seed_classes, [] )
    mapping = dict( ( mir[i], seed_classes[i] ) for i in range(len(mir)) )
    return mapping

def parse_yaml( file_name ):
    # Avoid changing code everytime by using configuration files
    from yaml import load, dump
    try:
        from yaml import CLoader as Loader, CDumper as Dumper
    except ImportError:
        from yaml import Loader, Dumper
    
    config = load( open(file_name, 'r'), Loader = Loader )
    return [config[x] for x in sorted( config.iterkeys() )]

def prepare_svm_rank_input( features, pos_mask, neg_mask ):
    # hybrid_mir = [ x.split('|')[-1] for x in features[:,1] ]
    # hybrid_mir = [ x.split(';') for x in hybrid_mir ]
    
    # mask = array([ features[x,0] in hybrid_mir[x] for x in range(len(features))])
    # mask1 = [True] * len(features)
    # for x in range(len(features)):
        # iden_seed = [mapping[y] for y in hybrid_mir[x]]
        # iden_seed = sum( iden_seed, [] )
        # mask1[x] = features[x,0] not in iden_seed
    
    # mask1 = array( mask1 )
    target = zeros( len(features), dtype = int )
    target[pos_mask] = 2
    target[neg_mask] = 1
    features[:,0] = target
    
    # To do: map read id to sorted integers
    seq_id = unique(features[:,1])
    id_to_int = dict()
    for i in range( len(seq_id) ):
        id_to_int[ seq_id[i] ] = str(i + 1)
    
    features[:,1] = [ id_to_int[x] for x in features[:,1] ]
    features = features[features[:,1].astype(int).argsort()]
    features[:,1] = [ 'qid:'+x for x in features[:,1] ]
    for i in range(3,34):
        features[:,i] = [ str(i-2)+':'+x for x in features[:,i] ]
    
    features = numpy.delete( features, [2, 34], 1 )
    features = numpy.delete( features, numpy.where( features[:,0] == '0' ), 0 )
    return features

def call_svm_rank( train_file, model_file, prediction_file, C = 10, eps = 1e-5 ):
    learn_cmd = ' '.join( ['svm_rank_learn', '-c', str(C), '-e', str(eps), train_file, model_file] )
    os.system( learn_cmd )
    predict_cmd = ' '.join( ['svm_rank_classify', train_file, model_file,
                           prediction_file] )
    os.system( predict_cmd )
    os.unlink( train_file )

def parse_svm_rank_output( model_file ):
    new_w = genfromtxt( model_file, delimiter = '#', dtype = 'string')
    new_w = new_w[-1]
    new_w = new_w.split( ' ' )
    new_w = new_w[ 1:len(new_w) ]
    new_w = array( [ x.split(':') for x in new_w ] )
    new_w = new_w[:,1].astype(float)
    new_w = new_w / norm(new_w)
    return new_w

def map_mir_to_seed_2( ref_f ):
    # Map each microRNA to a group of microRNAs with identical seed
    # Should generate directly from microRNA sequences in the future
    import numpy
    # mir_seq = numpy.genfromtxt( ref_f, delimiter=' ', dtype='string' )
    mir_seq = parse_fasta( ref_f )
    mir_seq = vstack( ( mir_seq.keys(), mir_seq.values() ) ).T
    mir_seq[:,1] = [ x[1:7] for x in mir_seq[:,1] ] 
    seed_classes = numpy.unique( mir_seq[:,1] )
    iden_seed = dict()
    for x in seed_classes:
        mask = array( mir_seq[:,1] == x )
        iden_seed[x] =  list( mir_seq[mask,0] )
    
    mapping = dict( ( mir_seq[i,0], iden_seed[ mir_seq[i,1]]) for i in
                   range(len(mir_seq)) )
    return mapping

# def run_alignment( aligner, conf_prefix, i, train_f, ref_f, out_prefix, type ):
    # cmd = " ".join([ 'perl', aligner, conf_prefix+str(i), train_f, ref_f,
                    # out_prefix+str(i), type ])
    # os.system( cmd )

def generate_mask( pos_features, pos_seq, ref_f ):
    import numpy
    from Bio.Seq import Seq
    # 1. Positive examples: miRNA names match
    hybrid_mir = [ x.split('|')[-1] for x in pos_features[:,1] ]
    hybrid_mir = [ x.split(';') for x in hybrid_mir ]
    pos_mask = array([ pos_features[x,0] in hybrid_mir[x] for x in range(len(pos_features))])
    # 2. Negative examples: miRNA names & seeds don't match 
    mapping = map_mir_to_seed_2( ref_f )
    mask1 = [True] * len(pos_features)
    for x in range(len(pos_features)):
        iden_seed = [mapping[y] for y in hybrid_mir[x]]
        iden_seed = sum( iden_seed, [] )
        mask1[x] = pos_features[x,0] not in iden_seed
    # 3. Negative examples: not containing other seed matches to make training easier
    # mir_seq = numpy.genfromtxt( ref_f, delimiter=' ', dtype='string' )
    mir_seq = parse_fasta( ref_f )
    mir_seq = vstack( ( mir_seq.keys(), mir_seq.values() ) ).T
    mir_seq[:,1] = [ str(Seq(x[1:7]).back_transcribe().reverse_complement()) for x in mir_seq[:,1] ] 
    seed_match = dict( ( mir_seq[i,0],  mir_seq[i,1] ) for i in range(len(mir_seq))  )
    mask2 = [ seed_match[ pos_features[x,0] ] not in pos_seq[ pos_features[x,1] ] for x in range( len(pos_features) ) ]
    neg_mask = array( [ mask1[x] and mask2[x] for x in range(len(pos_features)) ])
    return ( pos_mask, neg_mask )

def svm_load_serializable( svm_obj_file, suffix ):
    from shogun.Classifier import LibSVM
    from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File
    svm = LibSVM()
    fstream = SerializableAsciiFile( svm_obj_file + suffix, "r" )
    svm.load_serializable( fstream )
    return svm

def svm_apply( svm, seq ):
    from shogun.Features import StringCharFeatures, CombinedFeatures, DNA
    id = seq.keys()
    upstm_features = [ seq[x][0:22]  for x in id ] 
    dnstm_features = [ seq[x][28:50] for x in id ] 
    upstm_features = StringCharFeatures( upstm_features, DNA )
    dnstm_features = StringCharFeatures( dnstm_features, DNA )
    features = CombinedFeatures()
    features.append_feature_obj( upstm_features )
    features.append_feature_obj( dnstm_features )
    prediction = svm.apply( features )
    return { id[x] : prediction.get_value(x) for x in range( len(id) ) }

# If performance is so sensitive to gap penalties, maybe we should generate random initial gap penalties
# Need to enable setting ranges in function parameters
def random_init( file_name ):
    import random 
    f = open( file_name, "w" ) 
    for x in ( 'GT', 'TG', 'AT', 'TA', 'GC', 'CG' ):
        f.write( x + " " + "1" + "\n"  )
    # f.write( "open %.6f\n" % random.uniform( -1.5, -0.9 ) )
    # f.write( "sym %.6f\n" % random.uniform( .15, .3 ) )
    # f.write( "mirna %.6f\n" % random.uniform( .05, .2 ) )
    # f.write( "mrna %.6f\n" % random.uniform( -2, -1.25 ) )
    f.write( "open %.6f\n" % random.uniform( -2, -0.5 ) )
    f.write( "sym %.6f\n" % random.uniform( 0, .4 ) )
    f.write( "mirna %.6f\n" % random.uniform( 0, .4 ) )
    f.write( "mrna %.6f\n" % random.uniform( -2, -.5 ) )
    for x in range(21):
        v = "1" if x == 0 else "0"
        f.write( str(x) + " " + v + "\n" )
    f.close()


def slidingWindow(sequence,winSize,step=1):
    """Returns a generator that will iterate through the defined chunks of input sequence. 
    Input sequence must be iterable."""
    # Verify the inputs
    try: it = iter(sequence) 
    except TypeError: 
        raise Exception("**ERROR** sequence must be iterable.") 
    if not ((type(winSize) == type(0)) and (type(step) == type(0))): 
        raise Exception("**ERROR** type(winSize) and type(step) must be int.") 
    if step > winSize: 
        raise Exception("**ERROR** step must not be larger than winSize.")
    if winSize > len(sequence): 
        raise Exception("**ERROR** winSize must not be larger than sequence length.")
       # Pre-compute number of chunks to emit
    numOfChunks = ((len(sequence)-winSize)/step)+1
       # Do the work
    for i in range(0,numOfChunks*step,step): 
        yield sequence[i:i+winSize]

def model_load_serializable( model_class, model_obj_file, suffix ):
    import modshogun
    from shogun.IO import SerializableAsciiFile, MSG_DEBUG #, SerializableHdf5File
    model = getattr( modshogun, model_class )()
    fstream = SerializableAsciiFile( model_obj_file + suffix, "r" )
    model.load_serializable( fstream )
    return model

