import RNA
from utils import *

def find_pairing(s, ch):
    return [i for i, ltr in enumerate(s) if ltr == ch]

def count_features( mir, mrna, duplex ):
    mir_len = len( mir ) 
    from collections import Counter
    ( mir_pairing, mrna_pairing ) = duplex.structure.split('&')
    mir_coor = ( duplex.i - len(mir_pairing), duplex.i ) 
    mrna_coor = ( duplex.j - 1, duplex.j + len(mrna_pairing) - 1 ) 
    mir_idx = find_pairing( mir_pairing, '(' ) 
    mrna_idx = find_pairing( mrna_pairing, ')' )
    mir = mir[ mir_coor[0]:mir_coor[1] ]
    mrna = mrna[ mrna_coor[0]:mrna_coor[1] ]
    mrna_idx = mrna_idx[::-1]
    # First count base pairs
    pair_counts = [ mir[ mir_idx[i] ] + mrna[ mrna_idx[i] ] for i in range(len(mir_idx)) ]
    pad = Counter( { 'GT':0, 'TG':0, 'AT':0, 'TA':0, 'GC':0, 'CG':0 } )
    pad.update( Counter(pair_counts) )
    pair_counts = dict( pad ) 
    # Next count positions
    for i in range( mir_len ):
        pair_counts[ i ] = 1 if i in mir_idx else 0
    # Last count gaps ... How to do that though
    # Initialize
    gap_counts = { 'open':0, 'sym':0, 'mirna':0, 'mrna':0 }

    for i in parse_loop( duplex.structure.replace('&','') )[ 1: ]:
        if i == (0,0): continue
        else:
            gap_counts['open'] += 1
            gap_counts['sym'] += min( i )
            gap_counts['mirna'] += max( i[0] - i[1], 0 )
            gap_counts['mrna'] += max( i[1] - i[0], 0 )
    pair_counts = dict( pair_counts.items() +  gap_counts.items() )

    return pair_counts

def parse_loop( structure ):
    # Recursive
    try: 
        start = structure.index('(')
        end = structure.rindex(')')
    except ValueError:
        return [ (0,0) ]
    return [ ( start, len(structure) - end - 1 ) ]  + parse_loop( structure[start+1:end] ) 
    

def run_vienna( train_f, ref_f, out_f, all ):
    f = open( out_f, 'w' )
    mir_seq = parse_fasta( ref_f )
    mrna_seq = parse_fasta( train_f )
    for mrna_id in mrna_seq.keys():
        if all:
            hybrid_mir = mir_seq.keys()
        else:
            hybrid_mir = ( mrna_id.split('|')[-1] ).split(';')
        for mir_id in hybrid_mir:
            duplex = RNA.duplexfold( mir_seq[ mir_id ], mrna_seq[ mrna_id ] ) 
            duplex_score = -duplex.energy 
            count_dict = count_features( mir_seq[ mir_id ], mrna_seq[ mrna_id ], duplex )
            features = [ str(count_dict[x]) for x in [ 'GT', 'TG', 'AT', 'TA', 'GC', 'CG'] + [ 'open', 'sym', 'mirna', 'mrna' ] + range(21) ] 
            features = ','.join( features )
            f.write( mir_id + ',' + mrna_id + ',' + str(round( duplex_score, 6 )) + ',' + features + '\n' )
    f.close()

# run_vienna( '../training/hek293_clash_1mm_top_25_seed.fa', '../training/hek293_clash_mir_top_25_seed.fa', '../new_clash/clash.out', True )
# run_vienna( '../data/tcell.pos.no_155.with_pos.66nt.fa', '../data/tcell_clip_6mer_top_25_multi.fa', '../ce_clash/clip.pos.out', False )
# run_vienna( '../data/tcell.neg.no_155.with_pos.66nt.fa', '../data/tcell_clip_6mer_top_25_multi.fa', '../ce_clash/clip.neg.out', False )
