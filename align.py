import multiprocessing 
from utils import parse_fasta
from cProfile import run 
from align_output import AlignmentOutput
from functools import partial

try:
    from align_utils_max_gap import run_viterbi
except: 
    # If .so file is not there, compile it right now
    import pyximport
    pyximport.install()
    from align_utils_max_gap import run_viterbi


def readConfig(filename):
    positional_score = dict()
    # fields = genfromtxt(filename, dtype = 'string')
    config = open( filename, 'r' )
    fields = [ line.strip().split() for line in config ]
    for x in range(len(fields)):
        key = int( fields[x][0] ) if fields[x][0].isdigit() else fields[x][0]
        value = float(fields[x][1])
        positional_score[ key ] = value
    config.close()
    mirna_len = [x.__class__ for x in positional_score.keys()].count(int)
    bp_score = [ dict() for x in range(mirna_len) ]
    for y in ( 'AA', 'GA', 'CA', 'TA' ):
        bp_score[ 0 ][ y ] = positional_score[0]

    for x in range( 1, mirna_len ):
        for y in ( 'GT', 'TG', 'AT', 'TA', 'GC', 'CG' ):
            bp_score[ x ][ y ] = positional_score[ x ] + positional_score[ y ]

    return ( positional_score, bp_score )

def readMir( filename, mirna_len ):
    mirna_dict = parse_fasta( filename )
    mirna_dict = {k:v[0:mirna_len] for k,v in mirna_dict.iteritems() \
            if len(v) >= mirna_len}
    return mirna_dict



# ( positional_score, bp_score ) = readConfig('../baseline/hg19_1mm_50nt_centered_top_70.trans.config.9')
# mrna_dict = parse_fasta('/Users/yuhenglu/mirza/mir155.new.pos.fa')
# mirna_dict = { 'mmu-miR-155':'UUAAUGCUAAUUGUGAUAGGG' }
# mirna_dict = readMir( '/Users/yuhenglu/mirza/tcell_top100_mir.fa', 21 )

def pairwise_alignment( mrna_id, mrna_dict, mirna_dict, positional_score, bp_score, all = True ):
    output_list = list()
    if all:
        hybrid_mir = mirna_dict.keys()
    else:
        fields = mrna_id.split('|')
        hybrid_mir = fields[-1].split(';')
    for mirna_id in hybrid_mir:
        result = run_viterbi( mirna_dict[ mirna_id ], mrna_dict[ mrna_id ], positional_score, bp_score )
        result.setName( mirna_id, mrna_id )
        result.featureCount() 
        result.sanity( positional_score )
        # print result.display()
        # f.write( result.printFeature() + '\n' )
        output_list.append( result )
    return output_list

# run('test = pool.map( partial( pairwise_alignment, mrna_dict = mrna_dict, mirna_dict = mirna_dict, positional_score = positional_score, bp_score = bp_score, all = True ), mrna_dict.keys() )')



def run_alignment( conf_prefix, i, train_f, ref_f, out_prefix, all  ):
    ( positional_score, bp_score ) = readConfig( conf_prefix + str(i) )
    mirna_dict = readMir( ref_f, 21 )
    mrna_dict = parse_fasta( train_f ) 
    # Multi-core version 
    # Should add a single-core option as well?
    # max_num_process = multiprocessing.cpu_count()
    max_num_process = 8
    pool = multiprocessing.Pool( max_num_process )
    test = pool.map( partial( pairwise_alignment, mrna_dict = mrna_dict, mirna_dict = mirna_dict, positional_score = positional_score, bp_score = bp_score, all = all ), \
        mrna_dict.keys() ) 
    pool.close()
    pool.join()
    # map operation produces list of lists; unlist before any further operation...
    test = sum( test, [] )
    f = open( out_prefix + str(i), 'w' )
    for i in xrange( len( test ) ): 
        result = test[i]
        f.write( result.printFeature() + '\n' )
    f.close()
    return test

# tmp = run_alignment( conf_prefix = 'hg19_1mm_50nt_centered_top_70.trans.config.', \
        # i = 9, \
        # train_f = '/Users/yuhenglu/mirza/mir155.new.pos.fa', \
        # ref_f =  '/Users/yuhenglu/mirza/tcell_top100_mir.fa', \
        # out_prefix = 'test.out', all = True )
